ExternalData-add-httpheader
---------------------------

* The :module:`ExternalData` module gained a
  :variable:`ExternalData_HTTPHEADERS` variable to specify HTTP headers.
